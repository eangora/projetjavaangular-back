package org.sid.user;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.sid.dao.ContactRepository;
import org.sid.entities.Contact;
import org.springframework.beans.factory.annotation.Autowired;

public class Test {

	@Autowired
	private static ContactRepository contactRepository;
	
	public static void main(String[] args)   {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		try {
			contactRepository.save(new Contact("ANGORA", "Barth", df.parse("12/03/1986"), "ehueni@gmail.com","089675646", "photo.jpg"));
			contactRepository.save(new Contact("ANGORA4","Barth4", df.parse("12/03/1987"),"ehueni@gmail.com","098765422","photo.jpg"));
			contactRepository.findAll().forEach(c->{
				System.out.println(c.getNom());
			});
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//contactRepository.save(new Contact( "ANGORA", "Barth", df.parse("12/03/1986"), "ehueni@gmail.com", 0876543345, "photo.jpg"));
		

	}

}
