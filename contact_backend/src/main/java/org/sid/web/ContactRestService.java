package org.sid.web;

import java.util.List;

import org.sid.dao.ContactRepository;
import org.sid.entities.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/contact")
@CrossOrigin("*")
public class ContactRestService {
	
	@Autowired
	private ContactRepository contactRepository;
	//Voir tous les contacts
	//@RequestMapping(value="/contacts",method=RequestMethod.GET)
	@GetMapping("contacts")
	public List<Contact> getContacts(){
		return contactRepository.findAll();
	}
	
	//Voir les contacts par id
//		@RequestMapping(value="/cont/{id}",method=RequestMethod.GET)
	@GetMapping("contact/{id}")
		public Contact findById(@PathVariable(name="id") Long id){
			return contactRepository.findById(id).get();
		}
	
	//Creer contacts  localhost:8082/contact/contacts
	//body raw coller le json
	//@RequestMapping(value="/contacts",method=RequestMethod.POST)
	@PostMapping("contacts")
	public Contact save(@RequestBody Contact c){
		return contactRepository.save(c);
	}
//Supprimer un contact test avec postman localhost:8082/contact/contacte/5
	@DeleteMapping("contacte/{id}")
	public void supprimer(@PathVariable(name="id") Contact id){
		contactRepository.delete(id);
	}
	
	//Update un contact à partir de l'id
	@RequestMapping(value="contacts/{id}",method=RequestMethod.PUT)
	public Contact update(@PathVariable Long id, @RequestBody Contact c){
		//Contact c1 = contactRepository.findById(c.getId()).get();
		c.setId(id);
		return contactRepository.save(c);
	}
	
	@RequestMapping(value="chercherContacts",method=RequestMethod.GET)
	//@GetMapping("/chercherContacts")
		public Page<Contact> chercher(@RequestParam(name="mc",defaultValue="") String mc,
				@RequestParam(name="page",defaultValue="0")int page,
				@RequestParam(name="size",defaultValue="5")int size){
			return contactRepository.chercher("%"+mc+"%",PageRequest.of(page, size)
			);
		}
}
