package org.sid;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.sid.dao.ContactRepository;
import org.sid.entities.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContactApplication {
	@Autowired
	private ContactRepository contactRepository;
	public static void main(String[] args) {
		SpringApplication.run(ContactApplication.class, args);
	}
	
	public void run(String... args) throws Exception {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		contactRepository.save(new Contact("ANGORA", "Barth", df.parse("12/03/1986"), "ehueni@gmail.com","089675646", "photo.jpg"));
		//contactRepository.save(new Contact( "ANGORA", "Barth", df.parse("12/03/1986"), "ehueni@gmail.com", 0876543345, "photo.jpg"));
		contactRepository.save(new Contact("ANGORA4","Barth4", df.parse("12/03/1987"),"ehueni@gmail.com","098765422","photo.jpg"));
		contactRepository.findAll().forEach(c->{
			System.out.println(c.getNom());
		});
		
	}

}
